import Head from 'next/head';
import Image from 'next/image';
import styles from '../styles/Home.module.scss';
import Header from '../public/common/header';

export default function Home() {
	return (
		<div className={ styles['body'] }>
			<Head>
				<title>Stealth Design</title>
				<meta name="viewport" content="initial-scale=1.0, width=device-width" />
			</Head>
			<Header />
			<div className={ styles['tagline'] }>
				<h1>We design good experiences for fast growing companies globally. Get in touch</h1>
			</div>
			<div className={ styles['theme-video'] }>
				<video autoPlay muted loop style={{ width: '100%', height: '100%' }}>
					<source src={'/assets/home-video.mp4'} />
				</video>
			</div>
		</div>
	)
}
