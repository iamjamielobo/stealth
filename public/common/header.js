import Image from 'next/image';
import Link from 'next/link'

import logo from '../assets/stealth-logo.png';

import styles from '../../styles/Header.module.scss';

export default function Header() {
	return (
        <div className={ styles.header }>
            <div className={ styles.wrapper }>
                <div className={ styles['header-container'] }>
                    <div className={ styles.logo }>
                        <Link href='/'><a>
                            <Image
                                src={ logo }
                                alt="Stealth Design"
                                width={279}
                                height={60}
                                priority
                            />
                        </a></Link>
                    </div>
                    <nav className={ styles['nav'] }>
                        <ul className={`${styles['nav-list']} ${styles['aic']}`}>
                            <li className={ styles['nav-link'] }><Link href='/'><a>why stealth</a></Link></li>
                            <li className={ styles['nav-link'] }><Link href='/'><a>our work</a></Link></li>
                            <li className={ styles['nav-link'] }><Link href='/'><a>services</a></Link></li>
                            <li className={ styles['nav-link'] }><Link href='/'><a>join us</a></Link></li>
                            <li className={ styles['nav-link'] }><Link href='/'><a>get in touch</a></Link></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
	)
}
